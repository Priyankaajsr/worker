//=================================================
// AUTHOR: FNU Priyanka <fpriyanka@hawk.iit.edu>
// CREATE DATE: 03/15/2018
//=================================================

#ifndef WORKER_READER_H
#define WORKER_READER_H

#include <cstddef>
#include <string>
#include <list>
#include <libmemcached/memcached.h>
#include "structure.h"
#include "enumerations.h"

const std::string KEY_SEPARATOR="#";

class Reader{

private:
    memcached_st * mem_client;
    int application_id;

public:
    /**
     *
     * @param config_string
     * @param application_id
     */
    Reader(const char *config_string, int application_id){
        mem_client= memcached(config_string, strlen(config_string));
        this->application_id = application_id;
    }
    /**
     *
     * @param fileInput
     * @param destination
     * @return true if data read successfully else false
     */

    size_t read(input fileInput, std::list<std::string> destination);

//     /**
//      *
//      * @param fileInput
//      * @return
//      */
//     size_t read(input fileInput);
    /**
     *
     * @param source
     * @param sizeOfData
     * @return
     */

    std::string convertToString(void * source, size_t sizeOfData);

    /**
     *
     * @param fileInput
     * @return
     */
    size_t read(input fileInput);

    /**
     *
     * @param table_name
     * @param key
     * @param value
     * @return return true if data written to memcached else false
     */


    int put(std::string key,std::string value) ;
    /**
     *
     * @param table_name
     * @param key
     * @param value
     * @return
     */
    int put(table table_name,std::string key,std::string value);

    /**
     *
     * @param key
     * @return
     */
    std::string get(std::string key) ;

};

#endif //WORKER_READER_H
