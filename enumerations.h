//
// Created by Priyanka on 4/8/18.
//

#ifndef WORKER_ENUMERATIONS_H
#define WORKER_ENUMERATIONS_H

enum table{
    FILE_DB=0,
    FILE_CHUNK_DB=1,
    CHUNK_DB=2,
    SYSTEM_REG=3,
    DATASPACE_DB=4
};

#endif //WORKER_ENUMERATIONS_H
