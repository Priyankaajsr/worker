#include <iostream>
#include <chrono>
#include "reader.h"
#include "writer.h"
#include "delete.h"
#include "flusher.h"

int main() {

    const char *config_string = "--SERVER=localhost:11211";
    int application_id = 100;
    size_t result;
    Reader r(config_string,application_id) ;
    Writer w(config_string,application_id);
    Delete d(config_string);
    double elapsedLatencyTotal = 0.0;
    double elapsedReadTotal = 0.0;
    double elapsedLatencyReadTotal = 0.0;
    double elapsedLatencyWriteTotal = 0.0;
    double elapsedLatencyDeleteTotal = 0.0;
    double elapsedWriteTotal = 0.0;
    double elapsedCacheTotal = 0.0;
    double elapsedFileTotal = 0.0;
    for(int i = 4000000; i<5000000; i++){

        std::cout << "Cycle " << i << std::endl;
        input fileInput;
        fileInput.fileName = "/root/documents/example7.txt";
        fileInput.offset = 0 + i;
        fileInput.sizeOfData = 1000000;
        fileInput.source = (char*) malloc (sizeof(char)*fileInput.sizeOfData);
        r.read(fileInput);

        //Buffered file
        input fileInput1;
        std::string s = "/root/documents/testRead" + std::to_string(i) + ".txt";
        fileInput1.fileName = s.c_str();
        fileInput1.offset = 0;
        fileInput1.sizeOfData = 1000000;
        fileInput1.source = (char*)fileInput.source ;
        w.write(fileInput1);

        //Send it to MemcacheD (NATS Simulation replacement)
        auto start = std::chrono::steady_clock::now();
        std::string key = "fileInput" + std::to_string(i);
        std::string value = fileInput.fileName + std::to_string(fileInput.offset + i) + std::to_string(fileInput.sizeOfData);
        r.put(key,value);
        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double,std::milli> elapsedLatency = end - start;
        elapsedLatencyTotal = elapsedLatencyTotal + elapsedLatency.count();

        //Read from file and write to memcached
        auto start1 = std::chrono::steady_clock::now();
        input fileInputRead;
        std::string keyRead = "fileInput" + std::to_string(i);
        std::string valueRead= w.get(keyRead);
        std::string fileRead = "/root/documents/testRead" + std::to_string(i) + ".txt";
        fileInputRead.fileName = fileRead.c_str();
        fileInputRead.offset = 0;
        fileInputRead.sizeOfData = 1000000;
        fileInputRead.source = (char*) malloc (sizeof(char)*fileInputRead.sizeOfData);
        std::string dest1 = "test" + std::to_string(i);
        std::list<std::string> destination = {dest1};
        auto end1 = std::chrono::steady_clock::now();
        std::chrono::duration<double,std::milli > elapsedLatency1 = end1 - start1;
        elapsedLatencyReadTotal = elapsedLatencyReadTotal + elapsedLatency1.count();

        auto start5 = std::chrono::steady_clock::now();
        r.read(fileInputRead,destination);
        auto end5 = std::chrono::steady_clock::now();
        std::chrono::duration<double,std::milli > elapsedRead = end5 - start5;
        elapsedReadTotal = elapsedReadTotal + elapsedRead.count();

        //Read from memcached Write to file
        auto start6 = std::chrono::steady_clock::now();
        input fileInputWrite;
        std::string keyWrite = "fileInput" + std::to_string(i);
        std::string valueWrite= w.get(keyWrite);
        std::string fileWrite = "/root/documents/testWrite" + std::to_string(i) + ".txt";
        fileInputWrite.fileName = fileWrite.c_str();
        fileInputWrite.offset = 0;
      //  fileInputWrite.source = (char*) malloc (sizeof(char)*fileInputWrite.sizeOfData);
        std::string dest2 = "test" + std::to_string(i);
        std::list<std::string> destination2 = {dest2};
        auto end6 = std::chrono::steady_clock::now();
        std::chrono::duration<double,std::milli > elapsedLatencyWrite1 = end6 - start6;
        elapsedLatencyWriteTotal = elapsedLatencyWriteTotal + elapsedLatencyWrite1.count();

        auto start2 = std::chrono::steady_clock::now();
        w.write(destination2,fileInputWrite);
        auto end2 = std::chrono::steady_clock::now();
        std::chrono::duration<double,std::milli > elapsedWrite = end2 - start2;
        elapsedWriteTotal = elapsedWriteTotal + elapsedWrite.count();

        //Delete key from cache and File from Buffer

        auto start7 = std::chrono::steady_clock::now();
        std::string valueDelete= w.get(keyWrite);
        std::string fileR = "/root/documents/testRead" + std::to_string(i) + ".txt";
        std::string fileW = "/root/documents/testWrite" + std::to_string(i) + ".txt";
        std::string keyC = "100#0#test" + std::to_string(i);
        auto end7 = std::chrono::steady_clock::now();
        std::chrono::duration<double,std::milli > elapsedLatencyDelete = end7 - start7;
        elapsedLatencyDeleteTotal = elapsedLatencyDeleteTotal + elapsedLatencyDelete.count();
        d.Remove(key.c_str(),'C');

        auto start3 = std::chrono::steady_clock::now();
        d.Remove(keyC.c_str(),'C');
        auto end3 = std::chrono::steady_clock::now();
        std::chrono::duration<double,std::milli > elapsedCache = end3 - start3;
        elapsedCacheTotal = elapsedCacheTotal + elapsedCache.count();

        auto start4 = std::chrono::steady_clock::now();
        d.Remove(fileR.c_str(),'F');
        d.Remove(fileW.c_str(),'F');
        auto end4 = std::chrono::steady_clock::now();
        std::chrono::duration<double,std::milli > elapsedFile =  end4 - start4;
        elapsedFileTotal = elapsedFileTotal + elapsedFile.count();

    }

    std::cout << "Execution time elapsedLatency " << " " << elapsedLatencyTotal << " ms" << std::endl;
    std::cout << "Execution time elapsedLatencyRead " << " " << elapsedLatencyReadTotal << " ms" << std::endl;
    std::cout << "Execution time elapsedLatencyWrite " << " " << elapsedLatencyWriteTotal << " ms" << std::endl;
    std::cout << "Execution time elapsedLatencyDelete " << " " << elapsedLatencyDeleteTotal << " ms" << std::endl;
    std::cout << "Execution time elapsedRead" << " " << elapsedReadTotal << " ms" << std::endl;
    std::cout << "Execution time elapsedWrite" << " " << elapsedWriteTotal << " ms" << std::endl;
    std::cout << "Execution time elapsedCache" << " " << elapsedCacheTotal << " ms" << std::endl;
    std::cout << "Execution time elapsedFile" << " " << elapsedFileTotal << " ms" << std::endl;


    /*
     * For Reader
     */
//    for (int j = 0; j < 1000; ++j) {
//
//        input fileInput;
//        fileInput.fileName = "/root/documents/example7.txt";
//        fileInput.offset = 0 + j;
//        fileInput.sizeOfData = 1000000;
//        fileInput.source = (char*) malloc (sizeof(char)*fileInput.sizeOfData);
//        r.read(fileInput);
//
//        input fileInput1;
//        std::string s = "/root/documents/test" + std::to_string(j) + ".txt";
//        fileInput1.fileName = s.c_str();
//        fileInput1.offset = 0;
//        fileInput1.sizeOfData = 1000000;
//        fileInput1.source = (char*)fileInput.source ;
//        w.write(fileInput1);
//
//        auto start = std::chrono::steady_clock::now();
//        std::string key = "fileInput" + std::to_string(j);
//        std::string value = fileInput.fileName + std::to_string(fileInput.offset + j) + std::to_string(fileInput.sizeOfData);
//        r.put(key,value);
//        auto end = std::chrono::steady_clock::now();
//        std::chrono::duration<double,std::milli> elapsedLatency = end - start;
//        elapsedLatencyTotal = elapsedLatencyTotal + elapsedLatency.count();
//
//    }


//
//    for (int i = 0;i <100 ; i++){
//        input fileInput;
//        std::string s = "/root/documents/test" + std::to_string(i) + ".txt";
//        fileInput.fileName = s.c_str();
//        fileInput.offset = 0 + i;
//        fileInput.sizeOfData = 1000000;
//        fileInput.source = (char*) malloc (sizeof(char)*fileInput.sizeOfData);
//        std::string key = "fileInput" + std::to_string(i);
//        std::string value = fileInput.fileName + std::to_string(fileInput.offset) + std::to_string(fileInput.sizeOfData) + (char*)(fileInput.source );
//        r.put(key,value);
//    }

//    input fileInput;
//    std::list<std::string> destination = {"test12", "test13"};
//    auto start = std::chrono::steady_clock::now();
//    Reader r(config_string,application_id) ;
//    r.read(fileInput,destination);
//    auto end = std::chrono::steady_clock::now();
//    std::chrono::duration<double,std::milli > elapsed = end - start;
//    std::cout << "Execution time" << " " << elapsed.count() << " ms" << std::endl;


//    Reader r(config_string,application_id) ;
//    input fileInput;
//    fileInput.fileName = "/root/documents/example6.txt";
//    fileInput.offset = 0;
//    fileInput.sizeOfData = 1000000;
//    fileInput.source = (char*) malloc (sizeof(char)*fileInput.sizeOfData);
//
//    std::list<std::string> destination = {"test12", "test13"};
//    auto start = std::chrono::steady_clock::now();
//    r.read(fileInput,destination);
//    auto end = std::chrono::steady_clock::now();
//    std::chrono::duration<double,std::milli > elapsed = end - start;
//    std::cout << "Execution time" << " " << elapsed.count() << " ms" << std::endl;

    /*
     * For Writer
     */
//    Writer w(config_string,application_id);
//    input fileInput1;
//    std::list<std::string> source = {"abc", "xyz"};
//    size_t result1;
//    fileInput1.fileName = "example.txt";
//    fileInput1.offset = 10;
//    fileInput1.source = (char*) malloc (sizeof(char)*fileInput1.sizeOfData);
//    result1 = w.write(source,fileInput1);
    /*
     * For Delete
     */

//    Delete d(config_string);
//    const char * target = "example.txt";
//    int c = d.Remove(target,'F');
//    const char * target1 = "100#0#test";
//    int c1 = d.Remove(target1,'C');
//    const char * target2 = "/root/Test";
//    int c2 = d.Remove(target2,'D');

    /*
     * For Flusher
     */

//    input  fileInput[2];
//    fileInput[0].sizeOfData = 12;
//    fileInput[0].offset = 7;
//    fileInput[0].fileName = "example1.txt";
//    fileInput[0].source = (char*) malloc (sizeof(char)*fileInput[0].sizeOfData);
//    fileInput[1].sizeOfData = 2;
//    fileInput[1].offset = 20;
//    fileInput[1].fileName = "example1.txt";
//    fileInput[1].source = (char*) malloc (sizeof(char)*fileInput[1].sizeOfData);
////    fileInput[3] = NULL;
//    input fileOutput;
//    fileOutput.fileName = "example.txt";
//    fileOutput.offset = 7;
//    std::cout << sizeof(fileInput)/ sizeof(*fileInput) << std::endl;
//    Flusher f;
//    f.flush(fileInput,fileOutput,sizeof(fileInput)/ sizeof(*fileInput));

    return 0;
}