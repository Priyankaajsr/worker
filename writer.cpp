//=================================================
// AUTHOR: FNU Priyanka <fpriyanka@hawk.iit.edu>
// CREATE DATE: 03/17/2018
// PURPOSE: read data from file
// USAGE:
//=================================================

#include "writer.h"

size_t Writer::write(input fileInput) {
    FILE * pFile, * nFile;
    size_t result;
    pFile = fopen ( fileInput.fileName , "rb+");
    if(pFile == NULL){
        nFile = fopen ( fileInput.fileName , "wb+" );
        fseek ( nFile , fileInput.offset , SEEK_SET );
        result=fwrite(fileInput.source,1,fileInput.sizeOfData,nFile);
        fclose ( nFile );
    }else {
        fseek ( pFile , fileInput.offset , SEEK_SET );
        result=fwrite(fileInput.source,1,fileInput.sizeOfData,pFile);
        fclose ( pFile );
    }
    return result;
}

std::string Writer::get(std::string key) {
    char *return_value;
    size_t size;
    return_value = memcached_get(mem_client,
                                 key.c_str(),
                                 key.length(),
                                 &size ,
                                 (time_t)0,
                                 (uint32_t)0);
    if(return_value==NULL){
        return "";
    }
    return return_value;
}

std::string Writer::get(table table_name, std::string key) {
    char *return_value;
    size_t size;
    key=std::to_string(application_id)+KEY_SEPARATOR1+std::to_string(table_name)+KEY_SEPARATOR1+key;
    return_value = memcached_get(mem_client,
                                 key.c_str(),
                                 key.length(),
                                 &size ,
                                 (time_t)0,
                                 (uint32_t)0);
    if(return_value==NULL){
        return "";
    }
    return return_value;
}

size_t Writer::write(std::list<std::string> source, input fileInput) {

    size_t result;
    std::list<std::string>::const_iterator i;
    std::string value;
    table workerTable;
    for(i = source.begin(); i != source.end(); ++i){
        value = get(workerTable,i -> c_str());
        fileInput.source = &value[0];
        fileInput.sizeOfData = value.length();
        result = write(fileInput);
        fileInput.offset = fileInput.offset + fileInput.sizeOfData;
    }
    return result;
}

