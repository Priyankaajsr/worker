//=================================================
// AUTHOR: FNU Priyanka <fpriyanka@hawk.iit.edu>
// CREATE DATE: 03/18/2018
// PURPOSE: delete file amd folder
// USAGE:
//=================================================

#include "delete.h"

int Delete::deleteFile( const char * file) {

    return remove(file);

}

int Delete::deleteFolder(const char *folder) {

    DIR *dp;
    std::string filepath, dir;
    struct dirent *dirp;
    if(rmdir(folder)!= 0){
        dir = folder;
        dp = opendir(folder);
        if(dp == NULL)
            return -1;
        while(dirp = readdir( dp )){
            filepath = dir + "/" + dirp->d_name;
            deleteFile(filepath.c_str());
        }
        return rmdir(folder);
    }else
        return rmdir(folder);

}

int Delete::deleteCache(std::string key) {
    memcached_return_t rc = memcached_delete(mem_client, key.c_str(), key.length(), (time_t)0);
    return 0;
}

int Delete::Remove(const char *target, char flag) {

    switch(flag){
        case 'C': {
            deleteCache(target);
            break;
        }

        case 'F': {
            deleteFile(target);
            break;
        }

        case 'D': {
            deleteFolder(target);
            break;
        }

    }
    return  0;
}