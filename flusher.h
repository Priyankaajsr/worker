//=================================================
// AUTHOR: FNU Priyanka <fpriyanka@hawk.iit.edu>
// CREATE DATE: 04/14/2018
//=================================================

#ifndef WORKER_FLUSHER_H
#define WORKER_FLUSHER_H

#include <string>
#include "structure.h"
#include "enumerations.h"
#include <list>
#include <libmemcached/memcached.h>

class Flusher{

private:

public:

    /**
     *
     * @param fileInput
     * @param fileOutput
     * @return
     */

    size_t  flush( input fileInput[], input fileOutput, size_t size);

    /**
     *
     * @param fileInput
     * @return
     */
    size_t write(input fileInput);

    /**
     *
     * @param source
     * @param sizeOfData
     * @return
     */

    std::string convertToString(void * source, size_t sizeOfData);

    size_t read(input fileInput);




};

#endif //WORKER_FLUSHER_H
