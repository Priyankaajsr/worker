//
// Created by root on 4/15/18.
//
#include <sys/mount.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <string>

using namespace std;

int main()
{
    string host = "127.0.1.1";
    const char * src = "/C/Test";
    const char *  dst = "/root/Test";
    const char *  fstype = "cifs";

//    string all_string = "unc=" + src + ",ip=" + host + ",username=priyankaa.jsr@gmail.com,password= Priyanka9@";

    printf("src: %s\n", src);

//    std::string cmd = "mount -t " + fsType + " " + deviceName + " " + mountPoint;
//    int ret = system(cmd.c_str());



    if( -1 == mount(src, dst, fstype, 0, NULL))
    {
        printf("mount failed with error: %s\n",strerror(errno));
    }
    else
        printf("mount success!\n");

    if( umount2(dst, MNT_FORCE) < 0 )
    {
        printf("unmount failed with error: %s\n",strerror(errno));
    }
    else
        printf("unmount success!\n");


    return 0;
}