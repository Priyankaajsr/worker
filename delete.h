//=================================================
// AUTHOR: FNU Priyanka <fpriyanka@hawk.iit.edu>
// CREATE DATE: 03/18/2018
//=================================================

#ifndef WORKER_DELETE_H
#define WORKER_DELETE_H

#include <iostream>
#include <unistd.h>
#include <dirent.h>
#include <libmemcached/memcached.h>

class Delete{

private:
    memcached_st * mem_client;


public:
    Delete(const char *config_string){
        mem_client= memcached(config_string, strlen(config_string));
    }
    int deleteFile( const char * file);
    int deleteFolder(const char * folder);
    int deleteCache(std::string key);
    int Remove(const char * target, char flag);
};

#endif //WORKER_DELETE_H
