//=================================================
// AUTHOR: FNU Priyanka <fpriyanka@hawk.iit.edu>
// CREATE DATE: 03/17/2018
//=================================================

#ifndef WORKER_WRITER_H
#define WORKER_WRITER_H

#include <cstddef>
#include <string>
#include <list>
#include <libmemcached/memcached.h>
#include "enumerations.h"
#include "structure.h"

const std::string KEY_SEPARATOR1="#";

class Writer{

private:
    memcached_st * mem_client;
    int application_id;

public:
    /**
     *
     * @param config_string
     * @param application_id
     */
    Writer(const char *config_string, int application_id){
        mem_client= memcached(config_string, strlen(config_string));
        this->application_id = application_id;
    }

    /**
     * @param fileName
     * @return true if data written in the file successfully
     */
    size_t write(input fileInput);

    /**
     *
     * @param source
     * @param fileInput
     * @return
     */
    size_t  write(std::list<std::string> source, input fileInput);

    /**
     *
     * @param key
     * @return
     */
    std::string get(std::string key) ;

    /**
     *
     * @param table_name
     * @param key
     * @return
     */
    std::string get(table table_name, std::string key) ;

};

#endif //WORKER_WRITER_H
