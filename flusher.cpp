//=================================================
// AUTHOR: FNU Priyanka <fpriyanka@hawk.iit.edu>
// CREATE DATE: 04/14/2018
// PURPOSE: flush data to PFS
// USAGE:
//=================================================

#include "flusher.h"

size_t Flusher::flush(input fileInput[], input fileOutput, size_t size) {

    size_t result;
    std::string value;
    for(int i = 0; i< size; i++){
        read(fileInput[i]);
        size_t s = fileInput[i].sizeOfData;
        value = convertToString(fileInput[i].source,s);
        fileOutput.source = &value[0];
        fileOutput.sizeOfData = value.length();
        result = write(fileOutput);
        fileOutput.offset = fileOutput.offset + fileOutput.sizeOfData;
    }
    return result;

}

std::string Flusher ::convertToString(void *source, size_t sizeOfData) {
    char * buffer = (char*)source;
    std::string buf;
    for(int i = 0; i< sizeOfData; i++){
        if(buffer[i])
            buf = buf + buffer[i];
    }
    return buf;

}

size_t Flusher::write(input fileOutput) {
    FILE * pFile, * nFile;
    size_t result;
    pFile = fopen ( fileOutput.fileName , "rb+");
    if(pFile == NULL){
        nFile = fopen ( fileOutput.fileName , "wb+" );
        fseek ( nFile , fileOutput.offset , SEEK_SET );
        result=fwrite(fileOutput.source,1,fileOutput.sizeOfData,nFile);
        fclose ( nFile );
    }else {
        fseek ( pFile , fileOutput.offset , SEEK_SET );
        result=fwrite(fileOutput.source,1,fileOutput.sizeOfData,pFile);
        fclose ( pFile );
    }
    return result;
}

size_t Flusher::read(input fileInput) {
    FILE * pFile, * nFile;
    size_t result;
    pFile = fopen ( fileInput.fileName , "rb");
    if(pFile == NULL){
        result = false;
    }else {
        fseek ( pFile , fileInput.offset , SEEK_SET );
        result = fread(fileInput.source,1,fileInput.sizeOfData,pFile);
        fclose ( pFile );
    }
    return result;
}