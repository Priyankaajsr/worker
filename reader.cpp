//=================================================
// AUTHOR: FNU Priyanka <fpriyanka@hawk.iit.edu>
// CREATE DATE: 03/15/2018
// PURPOSE: read data from file
// USAGE:
//=================================================

#include "reader.h"
#include <iostream>

size_t Reader::read(input fileInput) {
    FILE * pFile, * nFile;
    size_t result;
    pFile = fopen ( fileInput.fileName , "rb");
    if(pFile == NULL){
        result = false;
    }else {
        fseek ( pFile , fileInput.offset , SEEK_SET );
        result = fread(fileInput.source,1,fileInput.sizeOfData,pFile);
        fclose ( pFile );
    }
    return result;
}

std::string Reader ::convertToString(void *source, size_t sizeOfData) {
    char * buffer = (char*)source;
    std::string buf;
    for(int i = 0; i< sizeOfData; i++){
        if(buffer[i])
            buf = buf + buffer[i];
    }
    return buf;

}

size_t Reader::read(input fileInput, std::list<std::string> destination) {

    size_t result;
    table workerTable;
    std::list<std::string>::const_iterator i;
    result = read(fileInput);
    if(result == fileInput.sizeOfData){
        for(i = destination.begin(); i != destination.end(); ++i){
//            std::cout<< (char*)fileInput.source << std::endl;
//            put(workerTable,i -> c_str(),convertToString(fileInput.source, fileInput.sizeOfData));
            put(workerTable,i -> c_str(),(char*)fileInput.source);
        }

    }

    return result;

}

int Reader::put(std::string key, std::string value) {

    memcached_return_t rc= memcached_set(mem_client,
                                         key.c_str(),
                                         key.length(),
                                         value.c_str(),
                                         value.length(),
                                         (time_t)0,
                                         (uint32_t)0);


    return 0;
}
int Reader::put(table table_name, std::string key, std::string value) {

    key=std::to_string(application_id)+KEY_SEPARATOR+std::to_string(table_name)+KEY_SEPARATOR+key;
    memcached_return_t rc= memcached_set(mem_client,
                                         key.c_str(),
                                         key.length(),
                                         value.c_str(),
                                         value.length(),
                                         (time_t)0,
                                         (uint32_t)0);

}

std::string Reader::get(std::string key)  {
    char *return_value;
    size_t size;
    return_value = memcached_get(mem_client,
                                 key.c_str(),
                                 key.length(),
                                 &size ,
                                 (time_t)0,
                                 (uint32_t)0);
    if(return_value==NULL){
        return "";
    }
    return return_value;
}