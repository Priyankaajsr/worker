//
// Created by Priyanka on 4/8/18.
//

#ifndef WORKER_STRUCTURE_H
#define WORKER_STRUCTURE_H

struct input{
    void * source;
    const char* fileName;
    long int offset;
    size_t sizeOfData;
};


#endif //WORKER_STRUCTURE_H
